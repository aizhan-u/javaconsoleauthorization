package Account;

import java.sql.Connection;
import java.util.Scanner;

public class AccountMain {

    public static int accountMain(Connection connection, Scanner scanner) {
        System.out.println("Do you have an account?\n0\tExit\n1\tYes\n2\tNo");
        int command = scanner.nextInt();
        int currentUserLoginId = 0;
        while(command != 0) {
            if(command == 1) {
                currentUserLoginId = Authorize.login(connection, scanner);
                if((currentUserLoginId = accountWork(connection, scanner, currentUserLoginId)) == 0) {
                    System.out.println("Do you have an account?\n0\tExit\n1\tYes\n2\tNo");
                    command = scanner.nextInt();
                } else {
                    break;
                }
            } else if(command == 2) {
                currentUserLoginId = createAccount(connection, scanner);
                break;
            }
        }
        return currentUserLoginId;
    }

    private static int accountWork(Connection connection, Scanner scanner, int currentUserLoginId) {
        int command;
        if(currentUserLoginId != 0) {
            System.out.println("What do you want to do?\n0\tLogout\n1\tChange password\n2\tChange profile info\n3\tDelete account\n4\tShop");
            command = scanner.nextInt();
            while (command != 0) {
                if (command == 1) {
                    LoginDB.changePassword(connection, scanner, currentUserLoginId);
                } else if (command == 2) {
                    for (; UserDB.updateProfileInfo(connection, scanner, currentUserLoginId) != 0; ) ;
                } else if (command == 3) {
                    UserDB.deleteProfile(connection, currentUserLoginId);
                    LoginDB.deleteAccount(connection, currentUserLoginId);
                    break;
                } else if (command == 4) {
                    return currentUserLoginId;
                }
                System.out.println("What do you want to do?\n0\tLogout\n1\tChange password\n2\tChange profile info\n3\tDelete account\n4\tShop");
                command = scanner.nextInt();
            }
        }
        return 0;
    }

    private static int createAccount(Connection connection, Scanner scanner) {
        int currentUserLoginId = LoginDB.signUp(connection, scanner);
        int command;
        if(currentUserLoginId != 0) {
            System.out.println("Do you want to add a profile info?\n1\tYes\n2\tNo");
            command = scanner.nextInt();
            if (command == 1) {
                UserDB.createProfile(connection, scanner, currentUserLoginId);
            } else {
                UserDB.createEmptyProfile(connection, currentUserLoginId);
            }
        }
        return currentUserLoginId;
    }
}
