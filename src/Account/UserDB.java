package Account;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class UserDB {
    static final String[] userCols = {"name", "surname", "birth_date in the format YYYY-MM-DD\nExample: 2021-04-13", "email"};

    private static int validateDate(String date) {
        int res = 0;
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(false);
        try {
            sdf.parse(date);
        } catch (ParseException e) {
            System.out.println("Incorrect format");
            res = -1;
        }
        return res;
    }

    private static int validateEmail(String email) {
        int res = 0;
        if(!email.matches("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$")) {
            System.out.println("Incorrect format");
            res = -1;
        }
        return res;
    }

    public static void createProfile(Connection connection, Scanner scanner, int loginId) {
        String[] vals = new String[4];
        String[] cols = {"name", "surname", "birth_date", "email", "login_id"};
        scanner.nextLine();

        for(int i = 0; i < vals.length; i ++) {
            System.out.println("Please, enter a value for " + userCols[i] + " or press 0 to exit");
            vals[i] = scanner.nextLine();

            if(vals[i].equals("0")) {
                createEmptyProfile(connection, loginId);
                return;
            }

            if(vals[i].equals("")) {
                System.out.println("No empty values allowed");
                i--;
            }

            if(i == 2) {
                i += validateDate(vals[i]);
            }

            if(i == 3) {
                i += validateEmail(vals[i]);
            }
        }

        insertIntoTable(connection, cols, vals, loginId);
    }

    public static void createEmptyProfile(Connection connection, int loginId) {
        String[] vals = {"", ""};
        String[] cols = {"name", "surname", "login_id"};
        insertIntoTable(connection, cols, vals, loginId);
    }

    private static void insertIntoTable(Connection connection, String[] cols, String[] vals, int loginId) {
        try {
            String query = "INSERT into users(";
            for(int i = 0; i < cols.length; i ++) {
                query += cols[i];
                if(i != cols.length - 1) {
                    query += ", ";
                }
            }
            query += ") VALUES (";
            for(int i = 0; i < vals.length; i ++)
                query = query + "'" + vals[i] + "', ";
            query = query + loginId + ");";

            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    public static int updateProfileInfo(Connection connection, Scanner scanner, int loginId) {
        System.out.println("Choose a value you want to change or 0 to return");
        System.out.println("Note: columns with \"id\" cannot be changed");
        String[] cols = showProfileInfo(connection, loginId);
        int col = scanner.nextInt();
        if(col == 0) {
            return 0;
        }

        if(col == 1 || col == 6) {
            System.out.println("I warned you");
            return 0;
        }

        System.out.println("Enter a new value for " + userCols[col - 2] + " or press 0 to break");
        scanner.nextLine();
        String val = scanner.nextLine();
        if(val.equals("0"))
            return 0;

        while(col == 4 && validateDate(val) == -1) {
            val = scanner.nextLine();
        }

        while(col == 5 && validateEmail(val) == -1) {
            val = scanner.nextLine();
        }

        while(col != 4 && col != 4 && val.equals("")) {
            System.out.println("No empty values allowed");
            val = scanner.nextLine();
        }

        updateTableValue(connection, cols[col - 1], val, loginId);
        return 1;
    }

    private static String[] showProfileInfo(Connection connection, int loginId) {
        String[] res = {};
        try {
            String query = "SELECT * from users where login_id = " + loginId + ";";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            int count = rsmd.getColumnCount();
            res = new String[count];

            if(rs.next()) {
                System.out.println("Column\tValue");
                for (int i = 1; i <= count; i++) {
                    res[i - 1] = rsmd.getColumnName(i);
                    System.out.println((i) + ": " + res[i - 1] + "\t\t" + rs.getString(i));
                }
            } else {
                System.out.println("No data found. Creating account with default values");
                createEmptyProfile(connection, loginId);
                return showProfileInfo(connection, loginId);
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return res;
    }

    private static void updateTableValue(Connection connection, String col, String newVal, int loginId) {
        try {
            String query = "UPDATE users set " + col + " = '" + newVal + "' where id = " + loginId + ";";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
            System.out.println("Update successful");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteProfile(Connection connection, int login) {
        try {
            String query = "delete from users where login_id = " + login + ";";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
