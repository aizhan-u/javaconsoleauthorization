package Account;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Base64;
import java.util.Scanner;

public class Authorize {
    public static int login(Connection connection, Scanner scanner) {
        int loginId = checkLogin(connection, scanner);
        if(loginId == 0)
            return 0;

        int tries = 0;
        String password;
        while(tries < 3) {
            System.out.println("Enter your password or press 0 to break");
            password = scanner.nextLine();
            if(password.equals("0"))
                return 0;
            if(checkPassword(connection, password, loginId)) {
                break;
            } else {
                tries += 1;
            }
        }

        if(tries == 3) {
            System.out.println("Too many tries");
            loginId = 0;
        }

        return loginId;
    }

    private static boolean checkPassword(Connection connection, String password, int loginId) {
        String passwordDB = getPassword(connection, loginId);
        String encodedPassword = Base64.getEncoder().encodeToString(password.getBytes());
        return encodedPassword.equals(passwordDB);
    }

    private static String getPassword(Connection connection, int loginId) {
        String password = "";
        ResultSet rs;
        try {
            String query = "SELECT password from logins where id = " + loginId + ";";
            Statement statement = connection.createStatement();
            rs = statement.executeQuery(query);
            rs.next();
            password = rs.getString(1);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return password;
    }

    private static int checkLogin(Connection connection, Scanner scanner) {
        int res = 0;
        String msg = "Enter your login or press 0 to return";
        scanner.nextLine();

        while(res == 0) {
            System.out.println(msg);
            String login = scanner.nextLine();
            if(login.equals("0"))
                return 0;
            res = checkLoginExists(connection, login);
            msg = "No login exists, try again";
        }
        return res;
    }

    private static int checkLoginExists(Connection connection, String login) {
        int res = 0;
        ResultSet rs;

        try {
            String query = "SELECT id from logins where login = '" + login + "';";
            Statement statement = connection.createStatement();
            rs = statement.executeQuery(query);
            if(rs.next()) {
                res = rs.getInt(1);
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return res;
    }
}
