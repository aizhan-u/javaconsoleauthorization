package main;

import Account.*;
import DBEstablishment.*;

import java.sql.Connection;
import java.util.Scanner;

public class
Main {
    public static void main(String[] args) {
        DBConnect dbConnect = new DBConnect();
        Connection connection = dbConnect.getConnection();
        //DropTables.dropTables(connection);
        CreateTables.createTables(connection);

        Scanner scanner = new Scanner(System.in);


        int currentUserLoginId = AccountMain.accountMain(connection, scanner);

        System.out.println("Your id: " + currentUserLoginId);
    }

}
