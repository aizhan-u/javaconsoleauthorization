package DBEstablishment;

import java.sql.Connection;
import java.sql.Statement;

public class CreateTables {

    public static void createTables(Connection connection) {
        createLoginsTable(connection);
        createUsersTable(connection);
        //createItemsTable(connection);
        //createUserCartTable(connection);
    }

    private static void createLoginsTable(Connection connection) {
        try {
            String query = "create table if not exists logins\n" +
                    "(\n" +
                    "    id         serial       not null\n" +
                    "        constraint logins_pk\n" +
                    "            primary key,\n" +
                    "    login      varchar(50) not null,\n" +
                    "    password      varchar(255) not null\n" +
                    ");\n" +
                    "\n" +
                    "alter table logins\n" +
                    "    owner to postgres;\n" +
                    "\n" +
                    "create unique index if not exists logins_login_uindex\n" +
                    "    on logins(login);\n";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void createUsersTable(Connection connection) {
        try {
            String query = "create table if not exists users\n" +
                    "(\n" +
                    "    id         serial       not null\n" +
                    "        constraint users_pk\n" +
                    "            primary key,\n" +
                    "    name       varchar(50) not null,\n" +
                    "    surname    varchar(50) not null,\n" +
                    "    birth_date date default '1900-01-01'::date not null,\n" +
                    "    email      varchar(50),\n" +
                    "    login_id    integer     not null,\n" +
                    "    constraint fk_login\n" +
                    "       foreign key(login_id)\n" +
                    "           references logins(id)\n" +
                    ");\n" +
                    "\n" +
                    "alter table users\n" +
                    "    owner to postgres;\n" +
                    "\n" +
                    "create unique index if not exists users_login_id_uindex\n" +
                    "    on users(login_id);\n";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void createItemsTable(Connection connection) {
        try {
            String query = "create table if not exists items\n" +
                    "(\n" +
                    "    id          serial       not null\n" +
                    "        constraint items_pk\n" +
                    "            primary key,\n" +
                    "    name        varchar(50) not null,\n" +
                    "    description varchar(255),\n" +
                    "    price       integer      not null\n" +
                    ");\n" +
                    "\n" +
                    "alter table items\n" +
                    "    owner to postgres;\n" +
                    "\n" +
                    "create unique index if not exists items_name_uindex\n" +
                    "    on items(name);\n";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void createUserCartTable(Connection connection) {
        try {
            String query = "create table if not exists user_cart\n" +
                    "(\n" +
                    "    id         serial       not null\n" +
                    "        constraint user_cart_pk\n" +
                    "            primary key,\n" +
                    "    login_id    integer     not null,\n" +
                    "    item_id    integer     not null,\n" +
                    "    count      integer     not null,\n" +
                    "    price      integer not null,\n" +
                    "    constraint fk_login\n" +
                    "       foreign key(login_id)\n" +
                    "           references logins(id),\n" +
                    "    constraint fk_item\n" +
                    "       foreign key(item_id)\n" +
                    "           references items(id)\n" +
                    ");\n" +
                    "\n" +
                    "alter table user_cart\n" +
                    "    owner to postgres;\n";
            Statement statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
