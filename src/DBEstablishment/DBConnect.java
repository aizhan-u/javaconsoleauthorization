package DBEstablishment;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnect {
    public Connection getConnection() {
        Connection connection = null;
        String host = "localhost";
        String port = "5432";
        String database = "postgres";
        String username = "postgres";
        String password = "1234";
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://" + host + ":" + port + "/" + database,username,password);

            if(connection != null) {
                System.out.println("Connection successful!");
            } else {
                System.out.println("Connection failed");
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        return connection;
    }
}
